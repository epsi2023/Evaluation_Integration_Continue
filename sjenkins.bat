@echo off

if "%1" == "start" (
    echo Démarrage de Jenkins...
    set JENKINS_HOME=C:\Users\delta\Documents\Jenkins\runtime\workspace
    java -jar %JENKINS_HOME%\jenkins.war --httpPort=8090 --prefix=/jenkins
) else if "%1" == "stop" (
    echo Arrêt de Jenkins...
    
) else (
    echo Utilisation: %0 [start/stop]
    exit /b 1
)
